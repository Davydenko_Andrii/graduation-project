#include "lis3dsh.h"
#include "lcd.h"
//------------------------------------
extern SPI_HandleTypeDef hspi1;
uint8_t buf2[8]={0};
char str1[30]={0};
extern uint8_t state;
extern int8_t bytes[8];
extern uint8_t flag;
extern uint8_t counter;
extern uint8_t pos;
//------------------------------------
static void Error (void)
{
	LD5_ON;
}
//--------------------------------------
uint8_t SPIx_WriteRead(uint8_t Byte)
{
	uint8_t receivedbyte = 0;
	if(HAL_SPI_TransmitReceive(&hspi1,(uint8_t*) &Byte,(uint8_t*) &receivedbyte,1,0x1000)!=HAL_OK)
	{
		Error();
	}
	return receivedbyte;
}
//--------------------------------------
void Accel_IO_Read(uint8_t *pBuffer, uint8_t ReadAddr, uint16_t NumByteToRead)
{
	if(NumByteToRead>0x01)
	{
		ReadAddr |= (uint8_t) (READWRITE_CMD | MULTIPLEBYTE_CMD);
	}
	else
	{
		ReadAddr |= (uint8_t)READWRITE_CMD;
	}
	CS_ON;
	SPIx_WriteRead(ReadAddr);
	while(NumByteToRead>0x00)
	{
		*pBuffer=SPIx_WriteRead(DUMMY_BYTE);
		NumByteToRead--;
		pBuffer++;
	}
	CS_OFF;
}
//--------------------------------------
void Accel_IO_Write(uint8_t *pBuffer, uint8_t WriteAddr, uint16_t NumByteToWrite)
{
	CS_OFF;
	if(NumByteToWrite>0x01)
	{
		WriteAddr |= (uint8_t) MULTIPLEBYTE_CMD;
	}
	CS_ON;
	SPIx_WriteRead(WriteAddr);
	while(NumByteToWrite>=0x01)
	{
		SPIx_WriteRead(*pBuffer);
		NumByteToWrite--;
		pBuffer++;
	}
	CS_OFF;
}
//--------------------------------------
uint8_t Accel_ReadID(void)
{
  uint8_t ctrl = 0;
	Accel_IO_Read(&ctrl,LIS3DSH_WHO_AM_I_ADDR,1);
	return ctrl;
}
//--------------------------------------
void Accel_AccFilterConfig(uint8_t FilterStruct)
{

}
//--------------------------------------
void AccInit(uint16_t InitStruct)
{
  uint8_t ctrl = 0;
	ctrl=(uint8_t)(InitStruct);
	Accel_IO_Write(&ctrl, LIS3DSH_CTRL_REG4_ADDR,1);
	ctrl=(uint8_t)(InitStruct>>8);
	Accel_IO_Write(&ctrl, LIS3DSH_CTRL_REG5_ADDR,1);
}
//--------------------------------------
void Accel_GetXYZ(int16_t* pData)
{
	int8_t buffer[6];
  uint8_t ctrl,i = 0x00;
	float sensitivity = LIS3DSH_SENSITIVITY_0_06G;
	float valueinfloat = 0;
	Accel_IO_Read(&ctrl, LIS3DSH_CTRL_REG5_ADDR,1);
	Accel_IO_Read((uint8_t*)&buffer[0], LIS3DSH_OUT_X_L_ADDR,1);
	Accel_IO_Read((uint8_t*)&buffer[1], LIS3DSH_OUT_X_H_ADDR,1);
	Accel_IO_Read((uint8_t*)&buffer[2], LIS3DSH_OUT_Y_L_ADDR,1);
	Accel_IO_Read((uint8_t*)&buffer[3], LIS3DSH_OUT_Y_H_ADDR,1);
	Accel_IO_Read((uint8_t*)&buffer[4], LIS3DSH_OUT_Z_L_ADDR,1);
	Accel_IO_Read((uint8_t*)&buffer[5], LIS3DSH_OUT_Z_H_ADDR,1);
	switch(ctrl&LIS3DSH__FULLSCALE_SELECTION)
	{
		case LIS3DSH_FULLSCALE_2:
			sensitivity=LIS3DSH_SENSITIVITY_0_06G;
			break;
		case LIS3DSH_FULLSCALE_4:
			sensitivity=LIS3DSH_SENSITIVITY_0_12G;
			break;
		case LIS3DSH_FULLSCALE_6:
			sensitivity=LIS3DSH_SENSITIVITY_0_18G;
			break;
		case LIS3DSH_FULLSCALE_8:
			sensitivity=LIS3DSH_SENSITIVITY_0_24G;
			break;
		case LIS3DSH_FULLSCALE_16:
			sensitivity=LIS3DSH_SENSITIVITY_0_73G;
			break;
		default:
			break;
	}
	for(i=0;i<3;i++)
	{
		valueinfloat = ((buffer[2*i+1] << 8) + buffer[2*i]);
		//*sensitivity;
		pData[i]=(int16_t)valueinfloat;
	}
}
//--------------------------------------
void Accel_ReadAcc(void)
{
	int16_t buffer[3]={0};
	int16_t xval, yval,zval = 0x0000;
	Accel_GetXYZ(buffer);
	xval=buffer[0];
	yval=buffer[1];
	zval=buffer[2];
	char str[10]={0};
	//sprintf(str1,"X:%06d Y:%06d Z:%06d\r\n", xval, yval, zval);
	//CDC_Transmit_FS((uint8_t*)str1,strlen(str1));
	buf2[0]=0x11;
	buf2[1]=0x55;
	buf2[2]=(uint8_t)(xval>>8);
	buf2[3]=(uint8_t)xval;
	buf2[4]=(uint8_t)(yval>>8);
	buf2[5]=(uint8_t)yval;
	buf2[6]=(uint8_t)(zval>>8);
	buf2[7]=(uint8_t)zval;
	//CDC_Transmit_FS(buf2,8);

	if((ABS(xval))>(ABS(yval)))
	{
		if(xval>6000)
		{
			LD5_ON;
		}
		else if(xval<-6000)
		{
			LD4_ON;
		}
	}
	else
	{
		if(yval>6000)
		{
			LD3_ON;
		}
		else if(yval<-6000)
		{
			LD6_ON;
		}
	}
	char kek;
	LCD_SetPos(0,1);
	LCD_String("bits:");
	for(int i=5;i<13;i++) {
	    LCD_SetPos(i, 1);
	    LCD_SendChar(bytes[i-5]+48);
	}
	float square = sqrt((zval * zval) + (xval * xval));
        int16_t res = -(atan((double)yval / square)) * 180. / M_PI;
        if(counter==8){
            int pow2=1;
               int sum=0;
               for (int i=8-1;i>-1;i--)
               {
                    if (bytes[i]==1)
                          sum+=pow2;
                    pow2*=2;
               }
               kek=(char)sum;
            counter = 0;
            LCD_SetPos(pos,0);
            LCD_SendChar(kek);
            pos++;
            for(int i=0;i<8;i++) {
        	bytes[i]=0;
            }
        }
        if(state==0&&res>22) {
            state=1;
	     bytes[counter]=1;
        }
        if(res==0&&state==1) {
            state=0;
            counter++;
        }
        if(state==0&&res<-22) {
                state = 1;
                bytes[counter]=0;
        }
        //sprintf(str, "%d", res);
        //LCD_Clear();
        //LCD_SetPos(1,0);
        //LCD_String(str);
	//HAL_Delay(20);
	LD3_OFF;
	LD4_OFF;
	LD5_OFF;
	LD6_OFF;
}
//--------------------------------------
uint8_t Accel_Ini(void)
{
	uint16_t ctrl = 0x0000;
	HAL_Delay(1000);
	uint8_t msg = Accel_ReadID();
	if(msg==0x3F) LD4_ON;
	else Error();
	ctrl = (uint16_t) (LIS3DSH_DATARATE_100 | LIS3DSH_XYZ_ENABLE);
	ctrl |= ((uint16_t) (LIS3DSH_SERIALINTERFACE_4WIRE|\
	LIS3DSH_SELFTEST_NORMAL|\
        LIS3DSH_FULLSCALE_2|\
	LIS3DSH_FILTER_BW_800))<<8;
	AccInit(ctrl);
	LD6_ON;
	return msg;
}
//--------------------------------------
